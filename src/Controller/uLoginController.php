<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 21.06.18
 * Time: 10:42
 */

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class uLoginController extends Controller
{
    const ULOGIN_DATA = 'ulogin-data';

    const NETWORKS = [
        'facebook' => 'faceBookId',
        'vkontakte' => 'vkId',
        'google' => 'googleId'
    ];



    /**
     * @Route("/register-case", name="app-register-case")
     *
     * @param UserRepository $userRepository
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function registerCaseAction(UserRepository $userRepository, ApiContext $apiContext)
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        if($this->checkNetworkId($userData['network'], $userData['uid'],$userRepository, $apiContext)){
            return $this->redirectToRoute('auth',[
                'error' => 'Эта учетная запись социальной связи уже привязана. Вы можете авторизоваться.'
            ]);
        }

        $user = new User();

        $email = $userData['email'] ?? '';
        $user->setEmail($email);


        $form = $this->createForm(
            'App\Form\ULoginRegisterType', $user, [
                'action' => $this
                    ->get('router')
                    ->generate('app-register-case2')
            ]
        );
        $this->get('session')->set(self::ULOGIN_DATA, $s);

        return $this->render('ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => null
        ]);
    }

    /**
     * @Route("/register-case2", name="app-register-case2")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function registerCase2Action(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $userData = json_decode(
            $this
                ->get('session')
                ->get(self::ULOGIN_DATA),
            true
        );

        $user = new User();

        $form = $this->createForm(
            'App\Form\ULoginRegisterType',
            $user
        );

        $form->handleRequest($request);

        $error = null;
        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $user->setPassword("social------------------------".time());
                    $user->setSocialId($userData['network'], $userData['uid']);
                    $data = $user->__toArray();

                    $apiContext->createClient($data);

                    $user = $userHandler->createNewUser($data);

                    $manager->persist($user);
                    $manager->flush();

                    return $this->redirectToRoute("auth", array(
                        'user' => $this->getUser()
                    ));
                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }
        }

        return $this->render('ul_2_state.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/login-case", name="app-login-case")
     *
     * @param ApiContext $apiContext
     * @param UserHandler $userHandler
     * @param UserRepository $userRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws ApiException
     */
    public function loginCaseAction(
        ApiContext $apiContext,
        UserHandler $userHandler,
        UserRepository $userRepository,
        ObjectManager $manager
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        $user = null;

        $user = $userRepository->getByNetwork(
            self::NETWORKS[$userData['network']],
            $userData['uid']
        );

        if ($user) {
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('start_page');
        }

        $user_central = $apiContext->getClientByNetwork(
            $userData['network'],
            $userData['uid']);

        if($user_central) {

            $user = $userRepository->getByEmail($user_central['email']);

            if($user){
                $user = $this->setNetworkId($userData['network'], $userData['uid'], $user);

                $manager->persist($user);
                $manager->flush();

                $userHandler->makeUserSession($user);
                return $this->redirectToRoute('start_page');
            }

            $user = $userHandler->createNewUser(
                $user_central,
                false
            );

            $manager->persist($user);
            $manager->flush();
            $userHandler->makeUserSession($user);
            return $this->redirectToRoute('start_page');
        }

        return $this->redirectToRoute('auth');
    }

    /**
     * @Route("/set-join-case", name="app-set-join-case")
     *
     * @param ApiContext $apiContext
     * @param UserRepository $userRepository
     * @param ObjectManager $manager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws ApiException
     */
    public function setJoinCaseAction(
        ApiContext $apiContext,
        ObjectManager $manager,
        UserRepository $userRepository
    )
    {
        $s = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $userData = json_decode($s, true);

        if($this->checkNetworkId($userData['network'], $userData['uid'], $userRepository, $apiContext)){
            return $this->redirectToRoute('client_cabinet',[
                'error' => 'Эта учетная запись социальной связи уже привязана. Вы можете авторизоваться.'
            ]);
        }

        $user = $this->getUser();

        $user = $this->setNetworkId($userData['network'], $userData['uid'], $user);

        if ($user) {
            $data = $user->__toArray();

            $apiContext->setNetworkToClient($data);

            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('client_cabinet');
        }

        return $this->redirectToRoute('auth');
    }


    /**
     * @param ApiContext $apiContext
     * @param string $network
     * @param string $identity
     * @param UserRepository $userRepository
     * @return bool
     * @throws ApiException
     */
    public function checkNetworkId(
        string $network,
        string $identity,
        UserRepository $userRepository,
        ApiContext $apiContext
    ){
        $busy = $userRepository->getByNetwork(
            self::NETWORKS[$network],
            $identity
        );

        if(!$busy){
            $busy = $apiContext->getClientByNetwork($network, $identity);
        }

        return ($busy ?? false);
    }

    /**
     * @param string $network
     * @param string $identity
     * @param User $user
     * @return User
     */
    public function setNetworkId(string $network, string $identity, User $user){
        switch ($network){
            case 'facebook':
                $user->setFaceBookId($identity);
                break;
            case 'vkontakte':
                $user->setVkId($identity);
                break;
            case 'google':
                $user->setGoogleId($identity);
                break;
        }

        return $user;
    }
}