<?php
/**
 * Created by PhpStorm.
 * User: kseniya
 * Date: 02.07.18
 * Time: 12:42
 */

namespace App\Controller;


use App\Model\Api\ApiContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RestPlaceController extends Controller
{

    /**
     * @Route("/catalog", name="catalog")
     *
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function indexAction(ApiContext $apiContext){

        $places = $apiContext->getRestPlaces();

        return $this->render('catalog.html.twig', [
            'places' => $places,
            'user' => $this->getUser()
        ]);
    }

    /**
     * @Route("/new_rest_place", name="new_rest_place")
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newRestPlaceAction(Request $request){

        $form = $this->createForm('App\Form\RestPlaceRegisterType');

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $data['landlord'] = $this->getUser()->getEmail();

            $request->getSession()->set('data', $data);

            return $this->redirectToRoute('other_parameters_rest_place');
        }

        return $this->render('new_rest_place.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/other_parameters_rest_place", name="other_parameters_rest_place")
     *
     * @param Request $request
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function addOtherParametersAction(Request $request, ApiContext $apiContext){

        $data = $request->getSession()->get('data');

        $builder = $this->createFormBuilder();

        if($data['type'] == 'hotel'){
            $builder->add('kitchen', ChoiceType::class, [
                'label' => 'Kitchen',
                'choices' => [
                    'Yes' => 1,
                    'No' => 0
                ],
                'expanded' => true
            ])
                ->add('selfPlage', ChoiceType::class, [
                'label' => 'SelfPlage',
                'choices' => [
                    'Yes' => 1,
                    'No' => 0
                ],
                'expanded' => true
            ]);
        } elseif ($data['type'] == 'sanatorium'){
            $builder->add('excursion', ChoiceType::class, [
                'label' => 'Excursion',
                'choices' => [
                    'Yes' => 1,
                    'No' => 0
                ],
                'expanded' => true
            ])
                ->add('treatment', ChoiceType::class, [
                'label' => 'Treatment',
                'choices' => [
                    'Yes' => 1,
                    'No' => 0
                ],
                'expanded' => true
            ]);
        }

        $builder->add('save', SubmitType::class);

        $form = $builder->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $form_data = $form->getData();
            if($data['type'] == 'hotel'){
                $data['kitchen'] = $form_data['kitchen'];
                $data['selfPlage'] = $form_data['selfPlage'];
            } elseif ($data['type'] == 'sanatorium'){
                $data['excursion'] = $form_data['excursion'];
                $data['treatment'] = $form_data['treatment'];
            }

            $apiContext->createRestPlace($data);
            return $this->redirectToRoute('catalog');
        }

        return $this->render('other_parameters_rest_place.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/booking/{restPlace}/{apartmentCount}", name="booking")
     *
     * @param int $restPlace
     * @param int $apartmentCount
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function showBookingAction(int $restPlace, int $apartmentCount, ApiContext $apiContext){

        $tenant = $this->getUser();

        if(!$apiContext->checkBookingByEmail($tenant->getEmail())){
            $variants = $apiContext->getBookingVariants($restPlace);

            $dates = [];

            foreach ($variants as $variant){
                $dates[] = [
                    'date' => date('d.m.Y', strtotime($variant['date'])),
                    'apartment' => $variant['apartment']
                ];
            }

            if(count($dates) < $apartmentCount){
                $dates[] = [
                    'date' => '01.06.2018',
                    'apartment' => (count($dates)+1)
                ];
            }



            return $this->render('booking.html.twig', [
                'dates' => $dates,
                'place' => $restPlace
            ]);
        }


        return $this->render('booking.html.twig', [
            'message' => 'Вы уже бронировали номер! Возможна только одна единовременная бронь!'
        ]);
    }


    /**
     * @Route("/booking/{restPlace}/register/{date}/{apartment}", name="booking_register")
     *
     * @param string $date
     * @param int $restPlace
     * @param int $apartment
     * @param ApiContext $apiContext
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \App\Model\Api\ApiException
     */
    public function registerBookingAction(string $date, int $restPlace, int $apartment, ApiContext $apiContext)
    {
        $data = [
            'restPlace' => $restPlace,
            'apartment' => $apartment,
            'tenant' => $this->getUser()->getEmail(),
            'startDate' => date('Y-m-d H:i:s', (strtotime($date) + 3600*24))
        ];


        if($apiContext->createBooking($data)){
            return $this->redirectToRoute('catalog');
        } else{
            return $this->render('booking.html.twig', [
                'message' => 'Что-то пошло не так!'
            ]);
        }
    }

}