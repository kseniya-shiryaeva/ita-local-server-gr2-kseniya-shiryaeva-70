<?php
namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    /**
     * @param ObjectManager $manager
     * @throws \App\Model\Api\ApiException
     */
    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewTenant([
            'email' => 'kkk@kkk.ru',
            'passport' => 'passport 111',
            'password' => '2d30c8d5d2a35fcce47c3db832646021573f5c43107691627f2708fc1e27f81a',
            'credit_card' => '555555',
            'roles' => ['ROLE_USER', 'ROLE_TENANT']
        ], false);

        $manager->persist($user);

        $user2 = $this->userHandler->createNewLandlord([
            'email' => 'ewq@qwe.ru',
            'passport' => 'qwerty & some',
            'password' => 'd8578edf8458ce06fbc5bb76a58c5ca44cc2321ca77b832bd20b66f86f85bef6',
            'full_name' => 'Vasya Dyadya',
            'roles' => ['ROLE_USER', 'ROLE_LANDLORD']
        ], false);

        $manager->persist($user2);

        $manager->flush();
    }
}
