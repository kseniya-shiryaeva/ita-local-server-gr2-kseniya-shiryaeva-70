<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RestPlaceRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('placeName')
            ->add('apartmentCount')
            ->add('contactPerson', TextType::class)
            ->add('phone', TextType::class)
            ->add('address', TextType::class)
            ->add('price', TextType::class)
            ->add('type', ChoiceType::class, [
                'label' => 'Type of rest place',
                'choices' => [
                    'Санаторий' => 'sanatorium',
                    'Отель' => 'hotel'
                ],
                'expanded' => true
            ])
            ->add('save', SubmitType::class);

    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'create_place';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'create_place';
    }
}
