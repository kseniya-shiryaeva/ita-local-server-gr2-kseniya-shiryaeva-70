<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_CLIENT_PASSWORD_ENCODE = '/client/password/encode';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';
    const ENDPOINT_CONCRETE_CLIENT_BY_NETWORK = '/client/{network}/network/{identity}';
    const ENDPOINT_SET_NETWORK_TO_CLIENT = '/set_network_to_client';
    const ENDPOINT_REST_PLACES = '/rest_places';
    const ENDPOINT_CREATE_REST_PLACE = '/rest_place';
    const ENDPOINT_CHECK_BOOKING_BY_EMAIL = '/check_booking/{email}';
    const ENDPOINT_GET_BOOKING_VARIANTS = '/booking_variants/{restPlace}';
    const ENDPOINT_CREATE_BOOKING = '/booking';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
            'passport' => $passport,
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $network
     * @param string $identity
     * @return array
     * @throws ApiException
     */
    public function getClientByNetwork(string $network, string $identity)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_NETWORK, [
            'network' => $network,
            'identity' => $identity
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->encodePassword($plainPassword),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function setNetworkToClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_SET_NETWORK_TO_CLIENT, self::METHOD_POST, $data);
    }

    /**
     * @param string $password
     * @return mixed
     * @throws ApiException
     */
    public function encodePassword(string $password)
    {
        $response = $this->makeQuery(self::ENDPOINT_CLIENT_PASSWORD_ENCODE, self::METHOD_GET, [
            'plainPassword' => $password
        ]);

        return $response['result'];
    }



    /**
     * @return array
     * @throws ApiException
     */
    public function getRestPlaces()
    {
        $endPoint = $this->generateApiUrl(self::ENDPOINT_REST_PLACES, []);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }



    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createRestPlace($data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_REST_PLACE, self::METHOD_POST, $data);
    }

    /**
     * @param $email
     * @return mixed
     * @throws ApiException
     */
    public function checkBookingByEmail($email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_BOOKING_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param $restPlace
     * @return mixed
     * @throws ApiException
     */
    public function getBookingVariants($restPlace)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_GET_BOOKING_VARIANTS, [
            'restPlace' => $restPlace
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createBooking(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CREATE_BOOKING, self::METHOD_POST, $data);
    }
}
